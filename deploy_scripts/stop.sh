#!/usr/bin/env bash

cd /home/ec2-user/node
pidraft=`pm2 pid RaFT`
echo "Pid is" $pidraft
if [[ ! -z $pidraft ]]
then
           echo "Killing process"
           pm2 delete all
fi
