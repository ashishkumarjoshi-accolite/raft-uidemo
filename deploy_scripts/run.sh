#!/usr/bin/env bash

cd /home/ec2-user/node
npm install
npm run build
echo "Starting"
pm2 --name RaFT start npm -- start
