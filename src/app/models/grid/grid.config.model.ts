export interface IGridConfig {
    height?: number;
    isQuickFilter?: boolean;
    headerHeight?: number;
    rowHeight?: number;
    gridName: string
}